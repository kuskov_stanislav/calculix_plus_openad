
      MODULE oad_intrinsics
      use w2f__types
      IMPLICIT NONE
      SAVE
C
C     **** Statements ****
C
      END MODULE

      SUBROUTINE fortfunc(A, B, RES, CSTRUCT)
      use w2f__types
      use oad_intrinsics
      IMPLICIT NONE
C
C     **** Global Variables & Derived Type Definitions ****
C
      TYPE  STRU
        REAL(w2f__8) A
        REAL(w2f__8) B
      END TYPE
      
C
C     **** Parameters and Result ****
C
      TYPE (oadactive) A
      REAL(w2f__8) B
      TYPE (oadactive) RES
      TYPE (STRU) CSTRUCT
C
C     **** Local Variables and Functions ****
C
      REAL(w2f__8) OpenAD_lin_0
C
C     **** Top Level Pragmas ****
C
C$OPENAD INDEPENDENT(A)
C$OPENAD DEPENDENT(RES)
C
C     **** Statements ****
C
      WRITE(*, *) __value__(A)
      WRITE(*, *) B
      OpenAD_lin_0 = (B *(__value__(A) **(B - INT(1_w2f__i8))))
      __value__(RES) = (__value__(A) ** B)
      CALL sax(OpenAD_lin_0, __deriv__(A), __deriv__(RES))
      WRITE(*, *) ' in fortfunc: res=   ', __value__(RES)
      END SUBROUTINE
