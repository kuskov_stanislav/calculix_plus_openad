/**********************************************************************/
/**
 * Copyright (c) 2015, Kshitij Kulshreshtha
 * Copyright (c) 2015, Sri Hari Krishna Narayanan

 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:

 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.

 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.

 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**********************************************************************/
/*! \file

 \brief Registration of ADOL-C external functions.

*/
/**********************************************************************/
#ifndef __AD_REGISTER_EXTERNAL_FUNCTIONS_HPP
#define __AD_REGISTER_EXTERNAL_FUNCTIONS_HPP

#include "ad_register_external_functions_export.hpp"
#include "ad_types.hpp"
//#include "ad_grad_driver.hpp"
//#include "ad_rev.hpp"
//#include "ad_tape.hpp"

void fortfunc_driver_c(double val, double deriv, double returnval, double returnderiv);

void ad_fortfunc_driver(OpenAD_forward *val, OpenAD_forward *deriv, OpenAD_forward *returnval, OpenAD_forward *returnderiv);

void ad_fortfunc_driver(OpenAD_reverse *val, OpenAD_reverse *deriv, OpenAD_reverse *returnval, OpenAD_reverse *returnderiv);



static ADOLC_ext_fct_v2 zosf_fortfunc_driver;

static ADOLC_ext_fct_v2_fos_forward fosf_fortfunc_driver;

static ADOLC_ext_fct_v2_fov_forward fovf_fortfunc_driver;

static ADOLC_ext_fct_v2_fos_reverse fosr_fortfunc_driver;

static ADOLC_ext_fct_v2_fov_reverse fovr_fortfunc_driver;

#endif /* #ifndef __AD_REGISTER_EXTERNAL_FUNCTIONS_HPP */
