/**********************************************************************/
/**
 * Copyright (c) 2015, Kshitij Kulshreshtha
 * Copyright (c) 2015, Sri Hari Krishna Narayanan

 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:

 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.

 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.

 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**********************************************************************/
/*! \file

 \brief Registration of ADOL-C external functions.

*/
/**********************************************************************/

#include "ad_external_functions_global_decls.hpp"
#include "ad_register_external_functions_internal.hpp"


int zosf_fortfunc_driver(int iArrLen, int *iArr, int nin, int nout, int *insz, double **invar, int *outsz, double **outvar, void*/*unused*/){ 
	fortfunc_driver_c(invar[0][0], invar[0][1], outvar[0][0], outvar[0][1]);
        return 0;
} 

int fosf_fortfunc_driver(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, double **xp, int *outsz, double **y, double **yp, void* ctx){
	fortfunc_driver_c((x[0][0]), (x[0][1]), (y[0][0]), (y[0][1]));
	std::cout << "  y[0][0]: " << y[0][0] << std::endl;
//               y[i][j] =  DERIV_val(outvar[i][j]);
// 	fovf_fortfunc_driver(iArrLen, iArr, nin, nout, insz, x, 1, &xp, outsz, y, &yp, ctx);  //ONLY SCALAR MODE FOR THE MOMENT
	return 0;
}

int fovf_fortfunc_driver(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, int ndir, double ***Xp, int *outsz, double **y, double ***Yp, void*/*unused*/){
// this is a test
/*	int i,j,k;
        static int firsttime = 1;
	ADIC_Init(ndir, 100);
	ADIC_SetForwardMode();
        ADIC_forward::DERIV_TYPE **invar, **outvar;
        invar = new  ADIC_forward::DERIV_TYPE *[nin];
        outvar = new  ADIC_forward::DERIV_TYPE *[nout];
	ADIC_forward::setIsAllocatedLocallyFlag(false);
        for (i = 0; i < nin; i++)
	  invar[i] = new ADIC_forward::DERIV_TYPE[insz[i]];
        for (i = 0; i < nout; i++)
	  outvar[i] = new ADIC_forward::DERIV_TYPE[outsz[i]];
	ADIC_forward::setIsAllocatedLocallyFlag(true);
	// Set indpendent variables' grad_array
        for (i = 0; i < nin; i++)
	  ADIC_CopyPointerOfIndepArrayFromSeed(invar, Xp, i, insz[i]); 
	// Set dependent variables' grad_array 
        for (i = 0; i < nout; i++)
       	  ADIC_CopyPointerOfDepArrayFromSeed(outvar, Yp, i, outsz[i]);     
	ADIC_SetIndepDone();
	// Initialize the value of the independent variables
        for (i = 0; i < nin; i++)
 	  for(j = 0; j < insz[i]; j++)
	  DERIV_val(invar[i][j]) = x[i][j];
	// Invoke AD function
	ad_fortfunc_driver(&(invar[0][0]), &(invar[0][1]), &(outvar[0][0]), &(outvar[0][1]));
	for(i = 0; i < nout; i++)
	  for(j = 0; j < outsz[i] ; j++)
            y[i][j] =  DERIV_val(outvar[i][j]);
        for (i = 0; i < nin; i++)
  	  delete [] invar[i];
        for (i = 0; i < nout; i++)
	  delete [] outvar[i];
	delete[] invar;
	delete[] outvar;
	ADIC_Finalize();
	return 0;
*/
}

int fosr_fortfunc_driver(int iArrLen, int* iArr, int nout, int nin, int *outsz, double **up, int *insz, double **zp, double **x, double **y, void* ctx){
	fovr_fortfunc_driver(iArrLen, iArr, nout, nin, outsz, 1, &up, insz, &zp, x, y, ctx);
	return 0;
}

int fovr_fortfunc_driver(int iArrLen, int* iArr, int nout, int nin, int *outsz, int dir, double ***Up, int *insz, double ***Zp, double **x, double **y, void*/*unused*/){
/*
        using namespace ADIC_reverse;
        int i,j,k;
        static int firsttime = 1;
	ADIC_Init(dir, 100);
        __ADIC_TapeInit();
	ADIC_SetReverseMode();
        ADIC_reverse::DERIV_TYPE **invar, **outvar;
        invar = new ADIC_reverse::DERIV_TYPE*[nin];
        outvar = new ADIC_reverse::DERIV_TYPE*[nout];
	ADIC_reverse::setIsAllocatedLocallyFlag(false);
        for (i = 0; i < nin; i++)
	  invar[i] = new ADIC_reverse::DERIV_TYPE[insz[i]];
        for (i = 0; i < nout; i++)
	  outvar[i] = new ADIC_reverse::DERIV_TYPE[outsz[i]];
	ADIC_reverse::setIsAllocatedLocallyFlag(true);
	// Set indpendent variables' grad_array
        for (i = 0; i < nin; i++)
	  ADIC_CopyPointerOfIndepArrayFromSeed(invar, Zp, i, insz[i]);
	// Set dependent variables' grad_array 
        for (i = 0; i < nout; i++)
       	  ADIC_CopyPointerOfDepArrayFromSeed(outvar, Up, i, outsz[i]);     
	ADIC_SetIndepDone();
        // Initialize the value of the independent variables
        for (i = 0; i < nin; i++)
 	  for(j = 0; j < insz[i]; j++)
	  DERIV_val(invar[i][j]) = x[i][j];
	// Invoke AD function in the forward sweep
        our_rev_mode.plain = 0; 
        our_rev_mode.adjoint = 0; 
        our_rev_mode.tape = 1; 
	ad_fortfunc_driver(&(invar[0][0]), &(invar[0][1]), &(outvar[0][0]), &(outvar[0][1]));
	// Invoke AD function in the reverse sweep
        our_rev_mode.plain = 0; 
        our_rev_mode.adjoint = 1; 
        our_rev_mode.tape = 0; 
	ad_fortfunc_driver(&(invar[0][0]), &(invar[0][1]), &(outvar[0][0]), &(outvar[0][1]));
        for (i = 0; i < nin; i++)
  	  delete [] invar[i];
        for (i = 0; i < nout; i++)
	  delete [] outvar[i];
        delete[] invar;
        delete[] outvar;
	ADIC_Finalize();
	return 0;
*/
}

ext_diff_fct_v2 * reg_ext_fct_fortfunc_driver(){ 
	// register external function and set up pointers
	ext_diff_fct_v2 *edf = reg_ext_fct(zosf_fortfunc_driver);
	edf->zos_forward = zosf_fortfunc_driver;
	// information for First-Order-Scalar (=fos) forward
	edf->fos_forward = fosf_fortfunc_driver;
	// information for First-Order-Vector (=fov) forward
	edf->fov_forward = fovf_fortfunc_driver;
	// information for First-Order-Scalar (=fos) reverse
	edf->fos_reverse = fosr_fortfunc_driver;
	// information for First-Order-Vector (=fov) reverse
	edf->fov_reverse = fovr_fortfunc_driver;
	return edf;
}


