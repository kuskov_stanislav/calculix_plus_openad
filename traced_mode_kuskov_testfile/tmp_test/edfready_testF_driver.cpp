
#include "ad_register_external_functions_export.hpp"
/*@ begin adic_extract @*/
void fortfunc_driver_c(cstruct1,cstruct2,cstructresult)
{

/* ADIC call is:
fortfunc_driver(outvar[0][0])
*/
/*@ begin adic_export interface
name='fortfunc_driver'

input = [ ('cstruct1',[],1)	]

output = [ ('cstructresult',[],1)     ]
@*/
#if defined(ADIC_ADOLC_TYPE) || defined(TAPENADE)
 static ext_diff_fct_v2 *edfct_fortfunc_driver = NULL;
 if (edfct_fortfunc_driver == NULL) {
   edfct_fortfunc_driver = reg_ext_fct_fortfunc_driver();
 }
 int iArrLen = 0;
 int *iArr = new int[iArrLen];
 int nin = 1;
 int nout = 1;
 int *insz = new int[nin];
 int *outsz = new int[nout];
 adouble **invar = new adouble*[nin];
 adouble **outvar = new adouble*[nout];
 adouble adcstruct1=cstruct1;
 invar[0]= &adcstruct1;
 insz[0]= 1;
 adouble adcstructresult=cstructresult;
 outvar[0]= &adcstructresult;
 outsz[0]= 1;
 call_ext_fct(edfct_fortfunc_driver,iArrLen,iArr,nin,nout,insz,invar,outsz,outvar);
 delete[] invar; delete[] outvar; delete[] insz; delete[] outsz; delete[] iArr;
#else

//do nothing literally

/*@ end adic_extract @*/

}
