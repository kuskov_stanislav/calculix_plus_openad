// ad_types.hpp


typedef double OpenAD_forward;
// #define OpenAD_forward::DERIV_TYPE double;
// typedef double OpenAD_forward::DERIV_TYPE;
typedef double OpenAD_reverse;

/*! \file

  \brief This file is for testing all pieces and all steps

  \authors Boyana Norris, Beata Winnicka, Paul Hovland
  \version $Id$

  Copyright (c) 2006, Argonne National Laboratory <br>
  All rights reserved. <br>
  See $ADIC_DIR/share/ADIC_Copyright.txt for details. <br>

*/

#ifndef __AD_TYPES_H
#define __AD_TYPES_H

/*
 * An ultra-simple adic header file for a static-memory vector gradient type.
 */
            
class DerivTypeClass{
public:
	double val;
	double *grad;
        DerivTypeClass();
        ~DerivTypeClass();
};
typedef DerivTypeClass DERIV_TYPE;

#define DERIV_val(a)      (a).val
#define DERIV_grad(a)     (a).grad
#define DERIV_gradref(a)  &((a).grad)
#define DERIV_TYPE_ref(a) &(a)

/* Include the first-derivative macros and functions header */
// #include "ad_grad.hpp"
#include <stdio.h>

#endif  /* #ifndef __AD_TYPES_H */




