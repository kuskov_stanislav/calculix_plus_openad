! program driver
      	subroutine fortfunc_driver(a,b,c,d)
	use OAD_active
	implicit none
	external testF
	real*8 a,b,c,d
	type(active) :: x, y,w1,w2
! 	x%v=.5D0
! 	y%v=.75D0
! 	x%d=1.3370D0
! 	a%v=1.0
! 	b%v=2.0
	x%v=a
	y%v=b
	x%d=c
	y%d=d
	call fortfunc(x,y,w1,w2)
	
! 	print *, 'driver running for x =',x%v
	print *, 'driver running for w1%v =',w1%v
	print *, 'driver running for w1%d =',w1%d
	print *, '  '
	print *, 'driver running for w2%v =',w2%v
	print *, 'driver running for w2%d =',w2%d
	print *, '  '
	print *, 'driver running for x%v =',x%v
	print *, 'driver running for x%d =',x%d
	print *, '  '
	print *, 'driver running for y%v =',y%v
	print *, 'driver running for y%d =',y%d
	
	print *, 'driver running for a =',a
	print *, 'driver running for b =',b
	print *, 'driver running for c =',c
	print *, 'driver running for d =',d
	!  print *, '            yields y =',y%v,' dy/dx =',y%d
! 	print *, '            r%v=   ', r%v
! 	print *, '            yields r =',r%v,' dr/dx =',r%d
! 	print *, 'analy value: x.*y+x*y.= ',x%d * y%v + y%d * x%v
	print *, 'analy value: x%d * y%v= ',x%d * y%v
	c=w1%v
	d=w1%d
      !  print *, '    1+tan(x)^2-dy/dx =',1.0D0+tan(x%v)**2-y%d
      ! end program driver

        return
        end

