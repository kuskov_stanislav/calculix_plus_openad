

// extern "C" {void fortfunc_(double*); }

// void fortfunc_driver(double cstruct1,double cstructresult)
void fortfunc_driver_c(double val,double deriv, double returnval,double returnderiv)
{
/*@ begin openad_export interface
name='fortfunc_driver'

input = [ ('val',[],1),('deriv',[],2) ]
output = [ ('returnval',[],3),('returnderiv',[],4) ]


@*/

// output = [         ]
// input = [ ('cstruct1',[],1),
//           ('cstructresult',[],2)	]
// 
// output = [ ('cstruct1',[],1),
//           ('cstructresult',[],2)        ]

//do nothing literally
// double tmp=1.0;
fortfunc_(&val, &deriv,&returnval,&returnderiv);


