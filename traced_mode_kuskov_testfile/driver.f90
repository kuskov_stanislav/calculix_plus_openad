program driver
  use OAD_active
  implicit none
  external testF
  type(active) :: x, y,r
  x%v=3.0D0
  y%v=5.0D0
  x%d=1.0D0
  call fortfunc(x,y,r)
  print *, 'driver running for x =',x%v
!  print *, '            yields y =',y%v,' dy/dx =',y%d
  print *, '		r%v=   ', r%v
  print *, '            yields r =',r%v,' dr/dx =',r%d
  print *, '     	analytic value:  x%d * y%v= ',x%d * y%v
!  print *, '    1+tan(x)^2-dy/dx =',1.0D0+tan(x%v)**2-y%d
end program driver

