

// #include "ad_types.hpp"
#include "ad_register_external_functions.h"
#include <adolc/adalloc.h>
#include <adolc/adolc.h>

int zosf_fortfunc_driver(int iArrLen, int *iArr, int nin, int nout, int *insz, double **x, int *outsz, double **y,void*){ 
  return zosf_fortfunc_driver_wrapped(iArrLen,  iArr,  nin,  nout,  insz,  x,  outsz,  y);
}

int fosf_fortfunc_driver(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, double **xp, int *outsz, double **y, double **yp,void*){
std::cout <<"Actually using adolc external functions..." << std::endl;
  return fosf_fortfunc_driver_wrapped( iArrLen,  iArr,  nin,  nout,  insz,  x,  xp,  outsz,  y,  yp);
}
int fovf_fortfunc_driver(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, int ndir, double ***Xp, int *outsz, double **y, double ***Yp,void*){	
	return fovf_fortfunc_driver_wrapped(iArrLen,  iArr, nin, nout,  insz,  x,  ndir,  Xp, outsz,  y, Yp);
}




int zosf_fortfunc_driver_wrapped(int iArrLen, int *iArr, int nin, int nout, int *insz, double **x, int *outsz, double **y){ 
	return 0;
}

int fosf_fortfunc_driver_wrapped(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, double **xp, int *outsz, double **y, double **yp){
	return 0;
}
int fovf_fortfunc_driver_wrapped(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, int ndir, double ***Xp, int *outsz, double **y, double ***Yp){
	
	return 0;
}


ext_diff_fct_v2  *reg_ext_fct_fortfunc_driver(){ 
        // register external function and set up pointers
        ext_diff_fct_v2 *edf = reg_ext_fct(zosf_fortfunc_driver);
        // information for Zero-Order-Scalar (=zos) forward
        // allocate memory for edf structure from n and m
        edf->zos_forward = zosf_fortfunc_driver;
        // information for First-Order-Scalar (=fos) forward
        edf->fos_forward = fosf_fortfunc_driver;
        // information for First-Order-Vector (=fov) forward
        edf->fov_forward = fovf_fortfunc_driver;
        return edf;
}



