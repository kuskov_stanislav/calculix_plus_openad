module OAD_intrinsics
end module
        subroutine fortfunc(a,b,res,cstruct)
        use OAD_intrinsics

! 	use ISO_FORTRAN_ENV
        type :: stru
          real*8:: a
          real*8 :: b
        end type stru
	
        TYPE(stru):: cstruct
        real*8 a
        real*8 b
        real*8 res
!$openad INDEPENDENT(a)
        print *, a
        print *, b
        res = a**b
!$openad DEPENDENT(res)
        print *, ' in fortfunc: res=   ', res
! 	write(6,100) a, b, res
! 100	format('a=',f6.3,' b=',f6.3, ' a*b= ',f9.3)

        return
        end
