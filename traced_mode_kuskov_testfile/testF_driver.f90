	function  fortfunc_driver(cstruct1,cstruct2,cstructresult)
! 	implicit none
	use OAD_active
	external testF
	
	
	! 	use ISO_FORTRAN_ENV
	type :: stru
	  real *8:: a
	  real *8:: b
	end type stru
	
	TYPE(stru):: cstruct1
	TYPE(stru):: cstruct2
	TYPE(stru):: cstructresult
	real*8 a
	real*8 b
	real*8 res
	
	type(active) :: x, y,r
	
! 	res=a*b
	
	x%v=cstruct1%a
	x%d=cstruct1%b
	
	y%v=cstruct2%a
	y%d=cstruct2%b


! 	x%v=.5D0
! 	y%v=.75D0
! 	x%d=1.3370D0
! 	r=x*y
	call fortfunc(x,y,r)
	
	print *, r
! 	print *, ' in fortfunc: a:   ', cstruct%a
! 	print *, ' correct value would have been 3.0'
! 	print *, ' in fortfunc: b:   ', cstruct%b 		
! 	print *, ' correct value would have been 4.0'
	
! 	print *, ' in fortfunc: res=   ', res 
	
	print *, '            yields r =',r%v,' dr/dx =',r%d
	print *, '     analytic value:  x%d * y%v= ',x%d * y%v
	cstructresult%a=r%v
	cstructresult%b=r%d	
	
! 	write(6,100) a, b, res
! 100	format('a=',f6.3,' b=',f6.3, ' a*b= ',f9.3)

	return
	end
