
     #ifndef __AD_REGISTER_EXTERNAL_FUNCTIONS_H
     #define __AD_REGISTER_EXTERNAL_FUNCTIONS_H
     
     #include <adolc/externfcts2.h>
     #include "active_def.h"
     
//      void fortfunc_driver_(active*,active*,active*); 
     ADOLC_ext_fct_v2 zosf_fortfunc_driver;
     ADOLC_ext_fct_v2_fos_forward fosf_fortfunc_driver; 
     ADOLC_ext_fct_v2_fov_forward fovf_fortfunc_driver; 
     ext_diff_fct_v2 *reg_ext_fct_fortfunc_driver();
     
     int zosf_fortfunc_driver_wrapped(int iArrLen, int *iArr, int nin, int nout, int *insz, double **x, int *outsz, double **y);
     int fosf_fortfunc_driver_wrapped(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, double **xp, int *outsz, double **y, double **yp);
     int fovf_fortfunc_driver_wrapped(int iArrLen, int* iArr, int nin, int nout, int *insz, double **x, int ndir, double ***Xp, int *outsz, double **y, double ***Yp);
     
     #endif /* #ifndef __AD_REGISTER_EXTERNAL_FUNCTIONS_H */
