
MODULE oad_intrinsics
use OAD_active
use w2f__types
IMPLICIT NONE
SAVE
!
!     **** Statements ****
!
END MODULE

SUBROUTINE fortfunc(A, B, RES, CSTRUCT)
use OAD_active
use w2f__types
use oad_intrinsics
IMPLICIT NONE
!
!     **** Global Variables & Derived Type Definitions ****
!
TYPE STRU
  REAL(w2f__8) A
  REAL(w2f__8) B
END TYPE

!
!     **** Parameters and Result ****
!
type(active) :: A
REAL(w2f__8) B
type(active) :: RES
type(STRU) :: CSTRUCT
!
!     **** Local Variables and Functions ****
!
REAL(w2f__8) OpenAD_lin_0
!
!     **** Top Level Pragmas ****
!
!$OPENAD INDEPENDENT(A)
!$OPENAD DEPENDENT(RES)
!
!     **** Statements ****
!
WRITE(*,*) A%v
WRITE(*,*) B
OpenAD_lin_0 = (B*(A%v**(B-INT(1_w2f__i8))))
RES%v = (A%v**B)
CALL sax(OpenAD_lin_0,A,RES)
WRITE(*,*) ' in fortfunc: res=   ',RES%v
END SUBROUTINE
